﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //Able to see scene management code
public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;
    public bool isGrounded;
    //For the ball so I can't spam jump

    int count;
    Rigidbody rb;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    void OnCollisionEnter(Collision col)
    {
        //If the ball is colliding with the tag ground is grounded = true and another jump can be made
        //if (col.gameObject.tag == ("Ground") && isGrounded == false)
        {
            //isGrounded = true;
            // Does not work because ground is not  "tagged"
        }

    }
    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);

        //If the space bar is pressed
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
            rb. GetComponent<Renderer>().material.color = new Color(Random.Range(0f,1f),Random.Range(0f,1f), Random.Range(0f, 1f));//When a cube is picked up the color of the ball changes
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 16)
        {
            winText.text = "You Win!";
            Invoke("RestartLevel", 3f);//restarts level after 3 seconds
        }

    
    }

    private void Update()
    {
        if (transform.position.y < -10f)
        {
            RestartLevel();
        }
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);//Reload level
    }
}